import React from "react";
import {
  Switch,
  Route,
  BrowserRouter as Router,
} from "react-router-dom";
import Layout from "./containers/Layout";
import Login from "./containers/Login";

export function App() {
  return (
    <Router>
      <Switch>
        {localStorage.getItem("token") ? <Layout /> : <Route exact path="/" component={Login} />}
      </Switch>
    </Router>
  );
}

export default App;