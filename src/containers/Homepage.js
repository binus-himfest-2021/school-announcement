import React, { useState, useEffect } from 'react';
import { getAnnouncements } from '../api/announcements';
import NewsCard from '../components/NewsCard';

export default function Homepage() {
  const [state, setState] = useState(
    {
      contents: []
    }
  )

  useEffect(() => {
    getAnnouncements().then(data => {
      const contents = [];
      if (data) {
        data.forEach(row => {
          contents.push({
            id: row.id,
            title: row.title,
            date: row.date,
            thumbnail: `${process.env.REACT_APP_BASE_API}${row.thumbnail.url}`,
            summary: row.summary,
            content: row.content,
          });
          setState({ contents });
        })
      }
    })
  }, []);

  return (
    <React.Fragment>
      {
        state.contents ? state.contents.map((content) => (
          <NewsCard
            key={content.id}
            date={content.subheader}
            thumbnail={content.thumbnail}
            title={content.title}
            summary={content.summary}
            content={content.content}
          />
        ))
        : null
      }
    </React.Fragment>
  );
}