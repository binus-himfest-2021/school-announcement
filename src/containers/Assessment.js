import React, { useState, useEffect } from 'react';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import {
  Paper,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Grid
} from '@material-ui/core';
import Title from '../components/Title';
import { getAssessment } from '../api/assessment';

const useStyles = makeStyles((theme) => ({
  seeMore: {
    marginTop: theme.spacing(3),
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },
}));

export default function Assessment() {
  const classes = useStyles();

  // function createData(id, date, name, shipTo, paymentMethod, amount) {
  //   return { id, date, name, shipTo, paymentMethod, amount };
  // }
  
  const [ state, setState ] = useState({
    rows: []
  })

  useEffect(() => {
    getAssessment().then(data => {
      const rows = [];
      if (data) {
        data.forEach(row => {
          rows.push({
            id: row.id,
            date: row.date,
            name: row.name,
            class: row.class,
            subject: row.subject,
            score: row.score,
          });
          setState({ rows });
        })
      }
    })
  }, []);

  return (
    <React.Fragment>
      <Grid item xs={12}>
        <Paper className={classes.paper}>
          <Title>Daftar Nilai</Title>
          <Table size="small">
            <TableHead>
              <TableRow>
                <TableCell>Tanggal</TableCell>
                <TableCell>Nama</TableCell>
                <TableCell>Kelas</TableCell>
                <TableCell>Pelajaran</TableCell>
                <TableCell align="right">Nilai</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {state.rows.map(row => (
                <TableRow key={row.id}>
                  <TableCell>{row.date}</TableCell>
                  <TableCell>{row.name}</TableCell>
                  <TableCell>{row.class}</TableCell>
                  <TableCell>{row.subject}</TableCell>
                  <TableCell align="right">{row.score}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
          <div className={classes.seeMore}>
            <Link color="primary" href="/">
              Kembali ke dashboard
            </Link>
          </div>
        </Paper>
      </Grid>
    </React.Fragment>
  );
}
