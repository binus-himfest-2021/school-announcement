import React, { useState } from 'react';
import LoginForm from '../components/LoginForm';
import { withRouter, useHistory } from "react-router-dom";
import { loginUser } from '../api/auth';

function Login() {
  const history = useHistory();

  const [state, setState] = useState({
    email: "",
    password: "",
  });

  function onChange(event) {
    setState({...state, [event.target.id]: event.target.value});
  };

  function onSubmit() {
    const credentials = {
      identifier: state.email,
      password: state.password,
    };
    loginUser(credentials).then(() => {
      history.go(0);
      history.push("/");
    });
  };

  return (
    <React.Fragment>
      <LoginForm email={state.email} password={state.password} onChange={onChange} onSubmit={onSubmit} />
    </React.Fragment>
  );
}

export default withRouter(Login);