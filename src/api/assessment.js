import { getApi } from "../utils/request";

export function getAssessment() {
  return getApi().get('assessments')
    .then(function (response) {
      return response.data;
    }).catch(function (err) {
      console.log(err)
    });
}
