import { getApi } from "../utils/request";

export function getAnnouncements() {
  return getApi().get('announcements')
    .then(function (response) {
      return response.data;
    }).catch(function (err) {
      console.log(err)
    });
}
