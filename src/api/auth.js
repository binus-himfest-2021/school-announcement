import { getApiNoAuth } from "../utils/request"

export function loginUser(credentials) {
  return getApiNoAuth().post('auth/local/', credentials)
    .then(function (response) {
      localStorage.setItem('token', response.data.jwt);
      localStorage.setItem('userId', response.data.id);
      return response.data;
    }).catch(function (err) {
      console.log(err);
    })
}

