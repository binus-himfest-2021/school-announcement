import axios from "axios";

export function getApiNoAuth() {
  return axios.create({
    baseURL: process.env.REACT_APP_BASE_API,
    timeout: 10000,
    withCredentials: true,
    headers: {
      'Content-Type': 'application/json'
    }
  });
}

export function getApi(params) {
  return axios.create({
    baseURL: process.env.REACT_APP_BASE_API,
    timeout: 100000,
    withCredentials: true,
    params:params,
    headers: {
      'accept': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`
    }
  });
}
